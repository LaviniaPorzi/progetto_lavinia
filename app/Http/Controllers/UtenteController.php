<?php

namespace App\Http\Controllers;

use App\Models\Utente;
use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;

class UtenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utentes =Utente::orderBy('created_at', 'DESC')->paginate(20);
        return view('utente.index', compact('utentes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('utente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterRequest $request)
    {
        Utente::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>$request->password,
        ]);

        return redirect(route('utenteIndex'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Utente  $utente
     * @return \Illuminate\Http\Response
     */
    public function show(Utente $utente)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Utente  $utente
     * @return \Illuminate\Http\Response
     */
    public function edit(Utente $utente)
    {
        return view('utente.edit', compact('utente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Utente  $utente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Utente $utente)
    {
        // $utente->update([
        //     'name'=>$request->name,
        //     'email'=>$request->email,
        //     'password'=>$request->password,
            $utente->name = $request->name;
            $utente->email = $request->email;
            $utente->password = $request->password;

            $utente->save();

        // ]);

        return redirect(route('utenteIndex'))->with('message', 'modificato con successo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Utente  $utente
     * @return \Illuminate\Http\Response
     */
    public function destroy(Utente $utente)
    {
        //
    }
}
