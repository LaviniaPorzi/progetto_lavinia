<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'il campo NAME non può essere vuoto',
            'email.required' => 'il campo EMAIL non può essere vuoto',
            'password.required' => 'il campo PASSWORD non può essere vuoto',
        ];
    }
}
