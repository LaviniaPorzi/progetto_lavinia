<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\UtenteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'welcome'] )->name('welcome');

//! rotta per il form utente
Route::get('/utente/create', [UtenteController::class, 'create'])->name('utenteCreate');
Route::post('/utente/store', [UtenteController::class, 'store'])->name('utenteStore');
Route::get('/utente/index', [UtenteController::class, 'index'])->name('utenteIndex');

Route::get('/utente/edit/{utente}', [UtenteController::class, 'edit'])->name('utenteEdit');
Route::put('/utente/update/{utente}', [UtenteController::class, 'update'])->name('utenteUpdate');
