<x-layout>

    <h2 class="p-5 text-center mid-title">Utenti</h2>
    <div class="container ">
        <div class="row  ">
            @foreach ($utentes as $utente)
            <div class="col-12 col-md-6 col-lg-4">
                <div class="card-1 my-3 text-center ">
                    <img src="https://picsum.photos/250/281" class="img-fluid " alt="...">
                    <div class="card-body ">
                      <h5 class="card-title">{{$utente->name}}</h5>
                      <p class="card-text">{{$utente->email}}</p>
                      <a href="{{route('utenteEdit', compact('utente'))}}" class="btn text-light px-4 my-3 btn-register">Modifica</a>
                    </div>
                </div>       
            </div>
            @endforeach
        </div>
        {{$utentes->links()}}
    </div>

</x-layout>