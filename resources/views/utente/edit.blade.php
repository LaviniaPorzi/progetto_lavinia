<x-layout>

    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-12 col-md-6">
                <h2 class="mid-title text-center my-5">Aggiorna dati</h2>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form class="register-form" method="POST" action="{{route('utenteUpdate', compact('utente'))}}">

                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                        <label class="form-label">Name</label>
                        <input type="text" class="form-control" name="name" value="{{$utente->name}}">
                    </div>
                   
                    <div class="mb-3">
                        <label class="form-label">Email address</label>
                        <input type="email" class="form-control" name="email" value="{{$utente->email}}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Password</label>
                        <input type="password" class="form-control" name="password" value="{{$utente->password}}">
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Confirm Password</label>
                        <input type="password" class="form-control" name="password_confirmation">
                    </div>
                    <button type="submit" class="btn text-light my-3 px-5 btn-register">Submit</button>
                  </form>
            </div>
        </div>
    </div>


</x-layout>