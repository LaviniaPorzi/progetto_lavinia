
@guest
<nav class="navbar navbar-expand-lg my-navbar">
    <div class="container-fluid">
      <a class="navbar-brand mx-5" ><i class="fa-solid fa-users fs-2 icon-nav"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        
        <ul class="navbar-nav ms-auto mx-5">
          
          <li class="nav-item">
            <a class="nav-link " aria-current="page" href="{{route('welcome')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('register')}}">Registrati</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('login')}}">Login</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('utenteCreate')}}">Crea Utente</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('utenteIndex')}}">I nostri utenti</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  @endguest

  @auth
  <nav class="navbar navbar-expand-lg my-navbar">
    <div class="container-fluid">
      <a class="navbar-brand mx-5" ><i class="fa-solid fa-users fs-2 icon-nav"></i></a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        
        <ul class="navbar-nav ms-auto mx-5">
          <li class="nav-item">
            <p class="fs-3 mx-5 color-red">Benvenutə: {{Auth::user()->name}} </p>
            
          </li>
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('welcome')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('utenteCreate')}}">Crea Utente</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('utenteIndex')}}">I nostri utenti</a>
          </li>
          <a class="nav-link fw-bold" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
              <form id="logout-form" action="{{route('logout')}}" method="POST" class="d-none">
                  @csrf
              </form>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  @endauth